Instructions et exemples pour le projet des MP2I du CIV
-------------------------------------------------------

Description du projet
=====================

Le projet « vacances d'hiver » consiste à réaliser un jeu.

Ce projet peut être réalisé par groupe de deux ou trois, sauf pour certains
élèves pour qui, afin que ce soit le plus profitable pour eux, devront le
réaliser seuls.

L'idée est de pratiquer l'informatique sur un programme avec plus de
*programmation élémentaire*, pas de révolutionner le monde du jeu vidéo.

Je vous suggère donc de partir sur un projet pas forcément très ambitieux et de
vous concentrer sur le gameplay plus que sur les graphismes. L'écueil à éviter
c'est de passer tout votre temps à faire un magnifique affichage et de n'avoir
aucun temps restant pour le jeu en lui-même. L'autre manière d'éviter de perdre
trop de temps est de ne pas faire un jeu *temps réel* mais en tour par tour.
Cela permet d'avoir une boucle interactive assez simple : on affiche, on lit
l'entrée, on calcule le prochain état du jeu, et on répète. L'avantage étant
que tout le monde joue par intervalle de temps discret, et c'est beaucoup plus
simple.

Quand on parle de jeu, cela peut-être un jeu de réflexion ou un casse-tête. La
dimension *ludique* n'est pas forcément la plus importante.

On essaiera de faire en sorte de dégager des parties algorithmiques
intéressantes :

* dans un jeu à deux joueurs cela peut être de faire une IA pour l'adversaire
* s'il y a un système de grille, réaliser des recherches de chemins, de
  visibilité, ...
* Développer une notion de données procédurales : niveaux, ennemis aléatoires

Des idées de jeux simples :

* Tetris ou autres
* Aventure en mode texte : Zork, ...
* Roguelike en console : Nethack, Dwarf Fortress, ...
* Jeux multi simples comme Bomberman

Installation d'OCaml
====================

On va avoir besoin d'utiliser `OCaml` avec un environnement permettant de
développer, d'installer des programmes et de compiler des exécutables.

* Sous Windows :
    * Installez WSL : Windows Subsystem for Linux. Vous pouvez 
    [suivre ces instructions](https://learn.microsoft.com/en-us/windows/wsl/install).
    * Vous avez maintenant une installation de Linux et vous pouvez suivre les
    instructions de Linux
* Sous Linux : il faut installer `opam` avec votre distribution. Sur ubuntu : `apt install opam`
* Sous OSX :
    * Installez https://brew.sh/
    * une fois que c'est fait vous pouvez installer `opam` avec 
      `brew install opam`

Une fois `opam` installé, on peut installer
sa propre mini distribution de `OCaml`.

Pour cela, dans un terminal, commencez par
lancer `opam init` pour initialiser 
la configuration d'`opam`, puis on va 
choisir une version de `OCaml` avec
`opam switch create 5.0.0` (elle vient
tout juste de sortir !).

Répondez `y` à tout. Quand vous relancez
un terminal, normalement, vous avez `OCaml` 5.0.0.

On pourra ensuite taper : `opam install dune utop`
et éventuellement d'autres bibliothèques au besoin.

Récupérer ces fichiers
======================

* Vous pouvez taper `git clone https://framagit.org/mdefalco/projet_mp2i.git`

Compiler des programmes avec `dune`
===================================

On va utiliser `dune` pour compiler des programmes. C'est un peu une variante
évoluée de `make` adaptée à `OCaml`. Dans le répertoire principal de votre
projet, on va créer un fichier `dune` qui a en général la structure suivante

```
(executable
    (name monprogramme)
    (libraries liste des bibliotheques utilisées))
```

Ici le point d'entrée du programme, cela peut être le seul fichier, est
`monprogramme.ml` dans le même répertoire que le fichier `dune`.

On crée aussi un fichier `dune-project` qui contient :

```
(lang dune 3.0)
```

Pour compiler, on se place à la racine du projet et on tape `dune build`.

Ça va créer un fichier `monprogramme.exe` dans le répertoire `_build/default`
créé lui aussi par `dune`. Pour le lancer on écrira donc
`./_build/default/monprogramme.exe`.


Par défaut, `dune` va compiler en considérant tous les avertissements comme étant des erreurs.
On peut rajouter les lignes suivantes au fichier `dune` pour supprimer des avertissements avec leur numéro.
Ici, on a supprimé les avertissements 27 et 26 :

``` ocaml
(env
  (dev
    (flags (:standard -w -27 -w -26))))
```

**Pour démarrer** il y a un répertoire déjà utilisable dans `template`.

Pistes pour faire des jeux avec OCaml
=====================================

### Mode texte
On peut très bien faire un jeu en mode texte avec des entrées et des sorties
purement textuelles. C'est le cas des jeux d'aventures en mode texte.
Par exemple, https://fr.wikipedia.org/wiki/Zork et les jeux similaires
peuvent être intéressant. Surtout avec une dimension lecture des entrées de
manière intelligente. C'est en partant de variante de ces jeux que le
concept de MOOC a été créé et qu'on a dérivé celui de MMORPG.

### Utiliser `graphics`

Cette bibliothèque est assez primitive mais suffisamment riche pour faire un
petit jeu. On trouvera un exemple dans le répertoire `graphics`. 

Il faut commencer par l'installer avec `opam install graphics`.

Deux exemples plus fournis dans `civhack` et `pacman`, ce dernier utilisant des
graphismes depuis un format d'image `bmp` avec un module de chargement auto-contenu.

### Utiliser la console comme un canevas

Avec la bibliothèque `curses` on peut utiliser la console comme un tableau
de pixels. Cette bibliothèque est très riche mais n'est pas forcément très
simple d'accès. Vous trouverez dans le répertoire curses un exemple avec 
des indications sur la manière d'obtenir de la documentation.

L'énorme avantage d'utiliser la console, c'est qu'on peut reposer sur
l'imagination des joueurs sans avoir à faire de graphismes. Par exemple,
`@` pourra représenter l'@vatar dans un jeu de role ou un jeu d'action.
C'est ce qui se passe dans les rogues comme `Nethack` ou `Dwarf Fortress`
qui ont inspirés beaucoup de jeux récents alors qu'ils sont très vieux.

**Installer curses** :

* sur les plateformes autre que `windows`, `opam install curses` suffit
* avec WSL, c'est-à-dire Linux dans Windows, ça doit bien marcher.


### (2022 Déconseillé) Utiliser la lib `sfml`

On l'installe avec `opam install sfml` et on va sur internet pour se
documenter. Attention, ici c'est une bibliothèque suffisante pour faire un jeu
commercialisable, donc il y a sûrement beaucoup de boulot pour l'utiliser, mais
le rendu final sera meilleur.

### **(2022 Conseillé)** Utiliser la lib `raylib`

A la fois très simple à mettre en œuvre et suffisamment complète pour faire un jeu commercialisable.

On l'installe avec `opam install raylib` et on peut consulter les examples
sur la page du portage : https://github.com/tjammer/raylib-ocaml.

Beaucoup de ressources en `C` qu'on peut directement adapter à `OCaml` sur le site https://www.raylib.com/examples.html.

L'avantage principal, c'est d'avoir la même bibliothèque en `C` et en `OCaml` : on apprend la logique une seule fois.

### Charger des images avec `imagelib`

* Installer avec `opam install imagelib`
* Dans le répertoire `images` un exemple où on charge un `bmp` et on l'affiche
* **A noter** que la boucle `graphics` est assez optimisée et peut servir de
  base.
* Vous pouvez réutiliser tel quel la fonction de conversion d'une image dans le
  format de la bibliothèque vers `graphics`.
* Dans le fichier dune il faut rajouter `unix imagelib imagelib.unix` dans les
  bibliothèques.

Rendu des projets
=================

Les projets sont à rendre le dimanche avant la rentrée par mail à l'adresse `projet@mpi.guru`.

Vos fichiers doivent être accompagnés d'un descriptif de ce que vous avez voulu
faire, de comment vous l'avez réalisé, etc...

Le code doit être clair et lisible : pas de fonctions de 2000 lignes...

Si vous travaillez seul :
-------------------------

Vous renvoyez par mail un `zip` de votre projet.

Si vous travaillez en groupe :
------------------------------

Vous devez utilisez `git` (voir instructions plus bas) et
vous envoyez par mail le lien du projet sur `framagit` (ou autre).


Coder avec git et framagit
==========================

**Note** on peut remplacer cela par `github` ou n'importe quel autre site. L'avantage de `gitlab` et donc de `framagit` est de permettre l'authentification par mot de passe en `https`, ce qui n'est pas une bonne idée pour un projet sur le long terme, mais suffisant pour démarrer.

**Mise en place**

* Allez sur `framagit.com` et créez un compte
  * si vous êtes le premier de votre groupe créez un repository initialisé avec un `README.md`
  * sinon, passez votre nom de compte à celui qui le gère pour qu'il vous donne les droits d'écriture
* Maintenant vous clonez le `repository` en choisissant l'adresse avec `https` dans le menu déroulant de `Code`.
  * par exemple vous allez taper `git clone https://framagit.org/mdefalco/advent-of-code-2020.git`.

Avec cette authentication, il va falloir taper votre username et votre mot de passe à chaque connexion sur le terminal.
On peut s'en passer avec une connexion `ssh`.

**Utilisation minimale**

Git fonctionne par lot de modification de fichiers, on appelle ça un `commit`. Ces modifications sont locales. Vous allez donc faire des modifications de vos programmes, quand vous êtes content faire un commit avec un message explicatif, envoyer ces modifs sur framagit ou recevoir les modifs des autres.

* Pour commiter toutes les modifications :
  * `git commit -a -m "un message descriptif"`
* Pour récupérer les dernières modifications :
  * `git pull`
  * **Attention** il ne faut pas que vous ayez de fichiers modifiés mais non commitées
* Pour envoyer vos modification :
  * `git push`
