open Random
open Graphics

type tile = Vide | MurHoriz | MurVert 
  | CoinBasDroite | CoinBasGauche | CoinHautDroite | CoinHautGauche
  | Sol | PorteFermee | PorteOuverte
  | Tunnel
  | EscalierMontant | EscalierDescendant 

type style = Gras of style | Ombre of style
  | Gris | Rouge | Vert | Bleu | Noir | Orange
  | BleuCiel | VertClair | Rose

type entite = { 
    mutable position : int * int;
    mutable vie : int;
    mutable vie_max : int;
    mutable travail : int;
    mutable travail_max : int;
    mutable force : int;
    mutable niveau : int;
    mutable etage : int;
    }

type npc = {
    npc_cat : string;
    npc_lettre : char;
    npc_style : style;
    mutable agressif : bool
    }

type objet = {
    objet_cat : string;
    objet_lettre : char;
    objet_style : style
    }

type jeu = { 
    mutable messages : string list;
    avatar : entite;
    mutable monstres : (entite * npc) list;
    etages : tile array array array
    }

(* Déclaration des types de données utilisés *)

let tile_vers_char = function
    Vide -> ' '
  | EscalierDescendant -> '>'
  | EscalierMontant -> '<'
  | MurHoriz -> '-'
  | MurVert -> '|'
  | CoinHautGauche 
  | CoinHautDroite
  | CoinBasGauche
  | CoinBasDroite -> '-'
  | Sol -> '.'
  | PorteFermee -> '+'
  | PorteOuverte -> '-'
  | Tunnel -> '#'

let combat attaquant defenseur = 
    let dommages = attaquant.force in
    defenseur.vie <- defenseur.vie - dommages;
    dommages


let gw,gh = 8, 13
(* Bas de l'ecran de la carte *)
(* Taille de l'ecran *)
let tw, th = 80, 24
let sbh = 2
let sby = 0
let messh = 1
let messy = th - 1
let mw, mh = tw, th - sbh - messh
let cx, cy = 0, sbh

let rec liste_suppr l m = match l with
    [] -> [] | x::q -> if x = m then q else x::(liste_suppr q m)

let rec liste_prefix l n = match n, l with
      0, _ -> []
    | _, [] -> []
    | _, t::q -> t :: (liste_prefix q (n-1)) 

let rec list_filter f l =
    match l with
        [] -> []
        | t::q -> if f t
            then t::(list_filter f q)
            else list_filter f q 


let putchar backgroundcolor color x y c = 
  moveto (cx+gw*x) (cy+gh*(mh-y-1));
  set_color backgroundcolor;
  fill_rect (cx+gw*x) (cy+gh*(mh-y-1)) gw gh;
  set_color color;
  draw_char c 
(* recupere une couleur codee sur 24bit et renvoie la luminosite moyenne *)
(* Gros hack, pour faire du contraste auto *)
let luminosite c =
    let b = c mod 256 in
    let g = (c / 256) mod 256 in
    let r= c / 65536 in
    (r+g+b)/3 

(* Pas du tout essentiel mais permet d'avoir une apparence plus sympathique de
    la carte *)
let puttile backgroundcolor color x y t =
  moveto (cx+gw*x) (cy+gh*(mh-y-1));
  set_color backgroundcolor;
  fill_rect (cx+gw*x) (cy+gh*(mh-y-1)) gw gh;
  set_color color;
  match t with 
    Vide -> ()
    | PorteOuverte -> 
        let pr = 2 in
        fill_rect (cx+gw*x+gw/2-pr) (cx+gh*(mh-y)-gh/2-pr) (2*pr) (2*pr)
    | MurHoriz -> 
        moveto (cx+gw*x) (cy+gh*(mh-y-1)+gh/2);
        lineto (cx+gw*(x+1)) (cy+gh*(mh-y-1)+gh/2)
    | MurVert ->
        moveto (cx+gw*x+gw/2) (cy+gh*(mh-y-1));
        lineto (cx+gw*x+gw/2) (cy+gh*(mh-y))
    | CoinHautGauche ->
        moveto (cx+gw*x+gw/2) (cy+gh*(mh-y-1));
        lineto (cx+gw*x+gw/2) (cy+gh*(mh-y-1)+gh/2);
        lineto (cx+gw*(x+1)) (cy+gh*(mh-y-1)+gh/2)
    | CoinHautDroite ->
        moveto (cx+gw*x+gw/2) (cy+gh*(mh-y-1));
        lineto (cx+gw*x+gw/2) (cy+gh*(mh-y-1)+gh/2);
        lineto (cx+gw*x) (cy+gh*(mh-y-1)+gh/2)
    | CoinBasGauche ->
        moveto (cx+gw*x+gw/2) (cy+gh*(mh-y));
        lineto (cx+gw*x+gw/2) (cy+gh*(mh-y-1)+gh/2);
        lineto (cx+gw*(x+1)) (cy+gh*(mh-y-1)+gh/2)
    | CoinBasDroite ->
        moveto (cx+gw*x+gw/2) (cy+gh*(mh-y));
        lineto (cx+gw*x+gw/2) (cy+gh*(mh-y-1)+gh/2);
        lineto (cx+gw*x) (cy+gh*(mh-y-1)+gh/2)
    | Sol -> 
        let pr = 1 in
        fill_rect (cx+gw*x+gw/2-pr) (cx+gh*(mh-y)-gh/2-pr) (2*pr) (2*pr)
    | Tunnel ->
        for i=cx+gw*x to cx+gw*(x+1) do
            for j=cy+gh*(mh-y-1) to cy+gh*(mh-y) do
                if (i/2 + j/2) mod 2 = 0 && i mod 2 = 0 && j mod 2 = 0 then plot i j
            done
        done
    | _ -> putchar backgroundcolor color x y (tile_vers_char t)

let piece m (x0,y0) (w,h) =
 for i = x0 to x0+w-1 do
  for j = y0 to y0+h-1 do
   let c = match i,j with
         a,b when a=x0 && b=y0 -> CoinHautGauche
        |a,b when a=x0 && b=y0+h-1 -> CoinBasGauche
        |a,b when a=x0+w-1 && b=y0 -> CoinHautDroite
        |a,b when a=x0+w-1 && b=y0+h-1 -> CoinBasDroite
        |_,b when b=y0 || b=y0+h-1 -> MurHoriz
        |a,_ when a=x0 || a=x0+w-1 -> MurVert
        |_ -> Sol in
    m.(i).(j) <- c
  done
 done

let tunnel_horiz m (x0,y0) l =
  for i = x0 to x0+l do
   m.(i).(y0) <- Tunnel
  done

let tunnel_vert m (x0,y0) l =
  for i = y0 to y0+l do
   m.(x0).(i) <- Tunnel
  done

let creer_entite x y vie travail force niveau etage =
    { position=x,y; vie=vie; vie_max=vie; travail=travail; travail_max=travail;
      force=force; niveau=niveau; etage=etage}

let lapin = { npc_cat="Lapin";npc_lettre='l'; npc_style=Gras Rouge; agressif=false }
let profdemaths = { npc_cat="Prof de maths";npc_lettre='M'; npc_style=Gras BleuCiel; agressif=true }
let profdephys = { npc_cat="Prof d'info";npc_lettre='I'; npc_style=Gras Vert; agressif=true }

let init_etage0 n jeu etage0  =
    tunnel_horiz etage0 (2,5) 70;
    List.iter (function a, b -> piece etage0 a b)
        [ (2,0),(10,5); (12,0),(10,5); (22,0),(10,5); (32,0),(10,5); (2,6),(10,5); (12,6),(10,5);
            (22,6),(10,5); (32,6),(12,8); (44,0),(4,5); (48,0),(4,5); (52,0),(4,5); (56,0),(4,5);
            (60,0),(4,5); (64,0),(4,5); (68,0),(4,5); (44,6),(5,8); (49,6),(5,8); (54,6),(5,8);
            (59,6),(10,5)];
    List.iter (function x,y -> etage0.(x).(y) <- PorteOuverte) 
        [ (61,6);(9,4);(19,4);(29,4);(41,3);
             (4,6);(14,6);(24,6); (34,13);
             (47,6); (46,4);(50,4);(54,4);
             (58,4);(62,4);(66,4);(70,4);
             (57,6);(52,6)];
    tunnel_horiz etage0 (32,14) 10;
    tunnel_horiz etage0 (32,6) 11;
    tunnel_vert etage0 (42,1) 4;
    tunnel_vert etage0 (43,1) 4;

    etage0.(40).(5) <- EscalierMontant; 

    jeu.monstres <- jeu.monstres @ 
        [creer_entite 3 1 4 10 1 1 n, lapin;
        creer_entite 3 2 4 10 1 1 n, lapin;
        creer_entite 3 3 4 10 1 1 n, lapin;
        creer_entite 4 1 4 10 1 1 n, lapin;
        creer_entite 4 2 30 10 1 1 n, profdephys;
        creer_entite 23 8 30 10 1 1 n, profdemaths;
        creer_entite 60 8 30 10 1 1 n, profdemaths]

let init_etage1 n jeu etage1  =
    tunnel_horiz etage1 (2,5) 70;
    List.iter (function a, b -> piece etage1 a b)
        [ (2,0),(10,5); (12,0),(10,5); (22,0),(10,5); 
            (32,0),(10,5); (2,6),(10,5); (12,6),(10,5);
            (22,6),(10,5); (44,0),(4,5); (48,0),(4,5);
            (52,0),(4,5); (56,0),(4,5);
            (60,0),(4,5); (64,0),(4,5);
            (68,0),(4,5); (49,6),(10,5);
            (59,6),(10,5)];
    List.iter (function x,y -> etage1.(x).(y) <- PorteOuverte) 
        [ (61,6);(9,4);(19,4);(29,4);(41,3);
             (4,6);(14,6);(24,6);
             (46,4);(49,4);(54,4);
             (58,4);(62,4);(66,4);(70,4);
             (51,6)];
    tunnel_horiz etage1 (32,6) 11;
    tunnel_vert etage1 (42,1) 4;
    tunnel_vert etage1 (43,1) 4;

    etage1.(40).(5) <- EscalierMontant; 
    etage1.(38).(5) <- EscalierDescendant; 

    jeu.monstres <- jeu.monstres @ 
        [creer_entite 3 1 4 10 1 1 n, lapin;
        creer_entite 3 2 4 10 1 1 n, lapin;
        creer_entite 3 3 4 10 1 1 n, lapin;
        creer_entite 4 1 4 10 1 1 n, lapin;
        creer_entite 4 2 30 10 1 1 n, profdephys;
        creer_entite 23 8 30 10 1 1 n, profdemaths;
        creer_entite 60 8 30 10 1 1 n, profdemaths]

let init jeu =
    init_etage0 0 jeu jeu.etages.(0);
    init_etage1 1 jeu jeu.etages.(1)

let affiche_map map = 
    for i = 0 to mw-1 do
     for j = 0 to mh-1 do
        puttile black white i j map.(i).(j)
     done
    done
    
let style_vers_color = function
  | Rouge -> rgb 255 0 0  
  | Vert -> rgb 0 255 0  
  | Bleu -> rgb 0 0 255
  | BleuCiel -> rgb 200 200 255
  | Gris -> rgb 127 127 127  
  | _ -> rgb 0 0 0
  
let rec putchar_in_style x y dx dy s c =
  match s with
   Gras sp -> putchar_in_style x y 0 0 sp c; 
     putchar_in_style x y 1 0 sp c
  | Ombre sp -> 
     putchar_in_style x y 0 1 Gris c; 
     putchar_in_style x y 0 0 sp c
  | _ -> 
    moveto (cx+gw*x+dx) (cy+gh*(mh-y-1)+dy);
    set_color (style_vers_color s);
    draw_char c

let affiche_avatar avatar =
    let x, y = avatar.position in
    putchar white black x y '@'

let affiche_monstre (entite,monstre) =
    let x,y = entite.position in  
    set_color black;
    fill_rect (cx+gw*x) (cy+gh*(mh-y-1)) gw gh;
    putchar_in_style x y 0 0 monstre.npc_style monstre.npc_lettre

let etage_max = 3 

let jeu = {
    messages=["Bienvenue dans CiVHack";
    		"Vous etes un nouvel eleve de MP2I";
    		"Votre objectif est de *survivre*";
    		"puis de parcourir le batiment des prepas";
    		"jusqu'au sommet de la plus haute tour";
    		"ou des generations de profs protegent";
    		"un artefact magique vous garantissant";
    		"reussite, prosperite et une bonne note";
    		"aux oraux de l'X"];
    etages=Array.init etage_max (fun _ -> Array.make_matrix mw mh Vide);
    monstres=[];
    avatar=creer_entite 42 14 15 10 3 1 0
    }

let affiche_statusbar jeu =
    let status = Printf.sprintf "HP:%03d/%03d Lvl:%02d Str:%02d %d,%d %d,%d"
        jeu.avatar.vie jeu.avatar.vie_max
        jeu.avatar.niveau jeu.avatar.force
        (fst jeu.avatar.position) (snd jeu.avatar.position)
        mw mh
    in 
    moveto 0 (sby*gh);
    set_color white;
    draw_string status 

let affiche_messages jeu =
    (* Zone qui n'est pas utilisee par la carte et qu'il faut donc mettre a
        zero *)
    set_color black;
    fill_rect 0 ((messy-2)*gh) (gw*tw) (3*gh);
    let rec aux i = function
            [] -> ()
        | m::l -> moveto 0 ((messy - i)*gh);
            set_color black;
            fill_rect 0 ((messy - i)*gh) (gw*tw) gh;
            set_color white;
            draw_string m;
            aux (i+1) l in
    aux 0 jeu.messages;
    let n = List.length jeu.messages in
    if n > messh
    then (
        set_color black;
        fill_rect 0 ((messy - n)*gh) (gw*tw) gh;
        moveto 0 ((messy - n) * gh);
        set_color white;
        draw_string "------Appuyez sur une touche pour continuer.";
        let _ = wait_next_event [Key_pressed] in
        jeu.messages <- liste_prefix jeu.messages messh;
        set_color black;
        fill_rect 0 ((messy-n) *gh) (gw*tw) ((n+1)*gh);
        set_color white;
        affiche_map jeu.etages.(jeu.avatar.etage);
        affiche_avatar jeu.avatar;
        List.iter affiche_monstre 
            (list_filter (fun (e,_) -> e.etage = jeu.avatar.etage) 
                     jeu.monstres);
        aux 0 (List.rev jeu.messages)
        )

let ajoute_messages jeu ml = 
    let mln = List.length ml in
    jeu.messages <- ml @ (liste_prefix jeu.messages (max 0 (messh - mln)))

let attend_confirmation jeu m =
    ajoute_messages jeu [m ^ " [on]"];
    affiche_messages jeu;
    let retour = ref true in
    let bcontinue = ref true in
    while !bcontinue do
        let s = wait_next_event [Key_pressed] in
        match s.key with
          'o' -> bcontinue := false
        | 'n' -> retour := false; bcontinue := false
        | _ -> ()
    done;
    ajoute_messages jeu ["D'accord"];
    affiche_messages jeu;
    !retour

let affiche_jeu jeu =
    affiche_map jeu.etages.(jeu.avatar.etage);
    affiche_avatar jeu.avatar;
    List.iter affiche_monstre (list_filter (fun (e,_) -> e.etage =
            jeu.avatar.etage) jeu.monstres);
    affiche_statusbar jeu;
    affiche_messages jeu

let case_libre_map x y =
    if x < 0 || y < 0 || x >= mw || y >= mh then false
    else match jeu.etages.(jeu.avatar.etage).(x).(y) with
      PorteFermee
    | MurHoriz | MurVert 
    | CoinHautGauche | CoinHautDroite 
    | CoinBasGauche | CoinBasDroite  
    | Vide -> false
    | _ -> true

let monstre_par_position x y =
    let rec aux = function [] -> None
        | m::l -> if (fst m).position = (x,y) && (fst m).etage = jeu.avatar.etage then Some m else aux l in
    aux jeu.monstres

let attaque_avatar_monstre (entite,npc) =
    let dommages = combat jeu.avatar entite in
    ajoute_messages jeu [Printf.sprintf 
        "Vous attaquez %s qui perd %d PV"
        npc.npc_cat dommages]

let cases_libres_autour x y =
    let cases = ref [] in
    for i=x-1 to x+1 do
        for j=y-1 to y+1 do
            (* oui il faut supprimer x y sinon le monstre se suicide.... *)
            if (i <> x || j <> y) && case_libre_map i j
            then cases := (i,j)::!cases
        done
    done;
    !cases

let random_choice l = 
    let n = List.length l in
    let r = int n in
    let rec nth l i = if i = 0 then List.hd l else nth (List.tl l) (i-1) in
    nth l r

let ia_stupide (entite,npc) =
    if entite.etage = jeu.avatar.etage
    then
        let x, y = entite.position in
        let nx, ny = random_choice (cases_libres_autour x y) in
        if jeu.avatar.position = (nx,ny)
        then begin
            if npc.agressif
            then let dommages = combat entite jeu.avatar in
                ajoute_messages jeu [Printf.sprintf "%s attaque. Vous perdez %d PV"
                    npc.npc_cat dommages]
            end
        else
            match monstre_par_position nx ny with
              None -> entite.position <- (nx,ny)
            | Some m -> if npc.agressif then let _ = combat entite (fst m) in ()

let bouge dx dy =
    let x, y = jeu.avatar.position in
    if case_libre_map (x+dx) (y+dy)
    then begin
        match monstre_par_position (x+dx) (y+dy) with
          None -> jeu.avatar.position <- (x+dx, y+dy)
        | Some m -> begin
            let attaque = 
                if (snd m).agressif 
                then true else attend_confirmation jeu "Ce monstre est pacifique. Attaquer ?"
            in
            if attaque then begin
                    attaque_avatar_monstre m;
                    (snd m).agressif <- true (* oui le lapin s'en souviendra *)
                end
            end
        end

let position_carte m c =
    let p = ref (0, 0) in
    for i = 0 to mw-1 do
        for j = 0 to mh-1 do
            if m.(i).(j) = c then p := (i,j)
        done
    done;
    !p

let descend_escalier () =
    let x,y = jeu.avatar.position in
    if jeu.etages.(jeu.avatar.etage).(x).(y) = EscalierDescendant
    then let nx, ny =position_carte 
            jeu.etages.(jeu.avatar.etage-1)
            EscalierMontant in
        jeu.avatar.position <- (nx,ny);
        jeu.avatar.etage <- jeu.avatar.etage - 1

let monte_escalier () =
    let x,y = jeu.avatar.position in
    if jeu.etages.(jeu.avatar.etage).(x).(y) = EscalierMontant
    then let nx, ny =position_carte 
            jeu.etages.(jeu.avatar.etage+1)
            EscalierDescendant in
        jeu.avatar.position <- (nx,ny);
        jeu.avatar.etage <- jeu.avatar.etage + 1

let ia_jeu jeu = List.iter ia_stupide jeu.monstres

let supprime_morts jeu =
    jeu.monstres <- list_filter (fun (e,_) -> e.vie > 0) jeu.monstres

let bouge_gauche () = bouge (-1) 0
let bouge_haut () = bouge 0 (-1)
let bouge_bas () = bouge 0 1
let bouge_droite () = bouge 1 0
let bouge_bg () = bouge (-1) 1
let bouge_bd () = bouge 1 1
let bouge_hg () = bouge (-1) (-1)
let bouge_hd () = bouge 1 (-1)


let boucle () = 
    let bcontinue = ref true in
    while !bcontinue do
        supprime_morts jeu;
        ia_jeu jeu;
        (* Les monstres peuvent s'attaquer donc il faut le refaire ici *)
        supprime_morts jeu;
        affiche_jeu jeu;
        let s = wait_next_event [Key_pressed] in
        match s.key with
        | '\027' -> bcontinue := false
        | 'h' -> bouge_gauche ()
        | 'j' -> bouge_bas ()
        | 'k' -> bouge_haut ()
        | 'l' -> bouge_droite ()
        | 'y' -> bouge_hg ()
        | 'u' -> bouge_hd ()
        | 'b' -> bouge_bg ()
        | 'n' -> bouge_bd ()
        | '<' -> monte_escalier ()
        | '>' -> descend_escalier ()
        | _ -> ()
    done

let _ = 
    open_graph (Printf.sprintf " %dx%d" (16+tw*gw) (50+th*gh));
    init jeu ;
    boucle () ;
    close_graph ()
