let w = 1000
let h = 1000

let setup () =
    Raylib.init_window w h "MP2I Test";
    Raylib.set_target_fps 60;
    []

let n_texts = 100
let r_dec = Array.init n_texts
    (fun _ -> (Random.int w, Random.int h))

let rec loop circles = 
    if Raylib.window_should_close () then Raylib.close_window ()
    else
        let open Raylib in
        begin_drawing ();
    clear_background Color.raywhite;

    let t = int_of_float (get_time () *. 100.) in

    for i = 0 to n_texts-1 do
        let rx, ry = r_dec.(i) in

        draw_text "Vive la MP2I!" 
            ((rx + t) mod w) ry 20
            Color.lightgray
    done;


    List.iter 
        (fun (x, y) -> 
            draw_circle x y 10. Color.black)
        circles;

    let mx, my = get_mouse_x (), get_mouse_y () in
    draw_circle mx my 10. Color.red;

    end_drawing ();

    let circles' =
        if is_mouse_button_down MouseButton.Left
        then begin
            (mx, my) :: circles
        end
        else circles in

    loop circles'

let () = 
    loop (setup ())
